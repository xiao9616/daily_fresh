from django.db import models

# Create your models here.
from db.base_model import BaseModel
from tinymce.models import HTMLField


class GoodsTest(BaseModel):
    STATUS_CHOICES = (
        (0, '下架'),
        (1, '上架')
    )
    status = models.SmallIntegerField(default=1, choices=STATUS_CHOICES, verbose_name='状态')
    content = HTMLField(verbose_name='内容')

    class Meta:
        db_table = 'good_test'


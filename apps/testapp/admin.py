from django.contrib import admin

# Register your models here.
from apps.testapp.models import GoodsTest

admin.site.register(GoodsTest)

from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic.base import View
from django.http import HttpResponse, JsonResponse
from apps.user.models import User
from daily_fresh import settings
from django.core.mail import send_mail


# def register(request):
#     if request.method == 'GET':
#         return render(request, 'register.html')
#     elif request.method == 'POST':
#         # 接收数据
#         username = request.POST.get('user_name')
#         pwd = request.POST.get('pwd')
#         email = request.POST.get('email')
#         allow = request.POST.get('allow')
#         # 数据效验
#         if not all([username, pwd, email]):
#             # 数据不完整
#             return render(request, 'register.html', {'errmsg': '数据不完整'})
#         import re
#         if not re.match(r'^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', email):
#             return render(request, 'register.html', {'errmsg': '邮箱格式不正确'})
#         if allow != 'on':
#             return render(request, 'register.html', {'errmsg': '请同意协议'})
#         # 业务处理
#         try:
#             user = User.objects.get(username=username)
#         except:
#             user = None
#         if user:
#             return render(request, 'register.html', {'errmsg': '用户名已存在'})
#
#         user = User.objects.create_user(username, email, pwd)
#         user.is_active = 0
#         user.save()
#         # 返回应答,跳转首页
#         return redirect('/goods/index')


class RegisterView(View):
    def get(self, request):
        return render(request, 'register.html')

    def post(self, request):
        username = request.POST.get('user_name')
        pwd = request.POST.get('pwd')
        email = request.POST.get('email')
        allow = request.POST.get('allow')
        # 数据效验
        if not all([username, pwd, email]):
            # 数据不完整
            return render(request, 'register.html', {'errmsg': '数据不完整'})
        import re
        if not re.match(r'^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', email):
            return render(request, 'register.html', {'errmsg': '邮箱格式不正确'})
        if allow != 'on':
            return render(request, 'register.html', {'errmsg': '请同意协议'})
        # 业务处理
        try:
            user = User.objects.get(username=username)
        except:
            user = None
        if user:
            return render(request, 'register.html', {'errmsg': '用户名已存在'})

        user = User.objects.create_user(username, email, pwd)
        user.is_active = 0
        user.save()

        # 发送激活邮件，包含用户的身份信息
        from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
        serializer = Serializer(settings.SECRET_KEY, 36000)
        info = {'confirm': user.id}
        token = serializer.dumps(info)
        token = token.decode()

        # 发送邮件
        subject = '天天生鲜'
        message = ''
        html_message = '<h1>%s,欢迎您成为天天生鲜注册会员</h1>请点击下面链接激活<br/> <a herf="http://127.0.0.1:8000/user/active/%s">http://127.0.0.1:8000/user/active/%s</a>' % (
            username, token, token)

        sender = settings.EMAIL_FROM
        recipient_list = [email]
        send_mail(subject=subject, message=message, from_email=sender, recipient_list=recipient_list,
                  html_message=html_message)
        # 返回应答,跳转首页
        return redirect('/goods/index')


class ActiveView(View):
    def get(self, request, token):
        from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
        serializer = Serializer(settings.SECRET_KEY, 36000)
        from itsdangerous import SignatureExpired
        try:
            info = serializer.loads(token)
            user_id = info['confirm']
            user = User.objects.get(id=user_id)
            user.is_active = 1
            user.save()
            return redirect('/user/login')
        except:
            return HttpResponse('激活链接已过期')


class LoginView(View):
    def get(self, request):
        return render(request, 'login.html')

    def post(self, request):
        # 接收数据
        username = request.POST.get('username')
        passwd = request.POST.get('pwd')

        if not all([username, passwd]):
            return render(request, 'login.html', {'errmsg': '数据不完整'})

        # user = authenticate(username=username, passwd=passwd)
        user = User.objects.get(username=username)
        pwd = user.password
        print(pwd,passwd)
        if pwd:
            if user.is_active:
                # 记录用户状态
                login(request, user)
                return redirect('/goods/index/')
            else:
                return render(request, 'login.html', {'errmsg': '未激活'})
        else:
            return render(request, 'login.html', {'errmsg': '用户密码错误'})
